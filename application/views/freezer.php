<?php $this->load->view('template/header'); ?>

<main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
      <div class="container">
        <h2>Freezer Van Directory</h2>
        <p>Here We suggested you some Freezer van </p>
      </div>
    </div><!-- End Breadcrumbs -->
</main>

<div class="container text-center mt-3">

    <div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="card">
            <div class="card-body">
                <h5 class="card-title">Van company Name</h5>
                <p class="card-text">Van Details</p>
                <p>Phone</p>
            </div>
            </div>
        </div>
    </div>
</div>



<?php $this->load->view('template/footer'); ?>